package com.tsystems.javaschool.tasks.calculator;



import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.LinkedList;



public class Calculator {


    static boolean isOperator(char c){
        return c == '+' || c == '-' || c=='*' || c=='/';
    }

    static int priority(char op){
        switch (op){
            case '+':
            case '-':
                return 1;
            case '*':
            case '/':
                return 2;
            default:
                return -1;
        }

    }
    static void processOperator(LinkedList<Double> st, char op) {
        double r = st.removeLast();     // take last element from list (stack)
        double l = st.removeLast();     // again
        switch (op) {                   // use operator between  l and r and put in list "st"
            case '+':
                st.add(l + r);
                break;
            case '-':
                st.add(l - r);
                break;
            case '*':
                st.add(l * r);
                break;
            case '/':
                st.add(l / r);
                break;
       }
    }

    static boolean isOtherSimbol(char c){
        String reg = "!@#$%^&\"\'; ,?\\`~=";
        return reg.indexOf(c)==-1 ? false : true;
    }




    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */

    public String evaluate(String statement) {
        // TODO: Implement the logic here
        String s ;
        LinkedList<Double> st = new LinkedList<>();         // a stack of numbers
        LinkedList<Character>  op = new LinkedList<>();     // a stack of operator
        char dot = ' ';

        if(statement == null || statement.isEmpty()) return null;

        for (int i = 0; i < statement.length(); i++) {
            char c = statement.charAt(i);

            if (c == '(')
                op.add('(');
            else if (c == ')') {
                if(op.contains('(')){
                while (op.getLast() != '(')                     //consider the expression in brackets
                    processOperator(st, op.removeLast());
                op.removeLast();}
                else return null;                               //exit from the method
            }

            else if (isOperator(c) ){
                if ( st.isEmpty() || isOperator(statement.charAt(i-1))){  //exit from the method
                    return null;}
                else {
                while (!op.isEmpty() && priority(op.getLast()) >= priority(c))
                        processOperator(st, op.removeLast());
                        op.add(c);}
            }

            else {
                String operand = "";

                //add the number to list st
                while (i < statement.length()){
                    if(Character.isDigit(statement.charAt(i))){
                        operand += statement.charAt(i++);
                    }
                    else if(statement.charAt(i)=='.' && dot ==' '  && operand.length()!=0){
                        //add digit after dot to operand
                        dot = statement.charAt(i);
                        operand +=statement.charAt(i++);
                    }
                    else if(statement.charAt(i)=='.' && dot =='.' ){
                        //exit from the method
                        return null;
                    }
                    else if(Character.isAlphabetic(statement.charAt(i)) || isOtherSimbol(statement.charAt(i)) )
                        //exit from the method
                        return null;
                    else
                        // exit while cycle
                        break;
                }
                --i;
                st.add(Double.parseDouble(operand));
                dot =' ';

            }

        }

        if(op.contains('(')) return null;
        else{
            while (!op.isEmpty())
            processOperator(st, op.removeLast());
        }

        Double result = st.get(0);

        if (result==Double.POSITIVE_INFINITY || result==Double.NEGATIVE_INFINITY){
            return null;}
        else {
            NumberFormat nf = new DecimalFormat("#.####");
            s = nf.format(result).replace(',','.');
        }

        return s;

    }

}
