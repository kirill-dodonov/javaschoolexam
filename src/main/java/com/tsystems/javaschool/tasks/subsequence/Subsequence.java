package com.tsystems.javaschool.tasks.subsequence;


import java.util.List;

public class Subsequence {


    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        // TODO: Implement the logic here
        boolean p = false;
        if(x==null || y==null) throw new IllegalArgumentException();

        if(x.isEmpty() ) return true;

        else if (y.isEmpty() || x.size() > y.size() ) return false;

        else {
            int j=0;
            int i=0;
            for ( ;i < x.size();) {
                for ( ;j < y.size(); ) {

                    if(y.get(j)==x.get(i)){
                        i++;
                        j++;}
                    else if (y.get(j)!=x.get(i)){j++;}

                    if(j==y.size()-1 && i!=x.size()-1 ){return false;}
                    if(i==x.size()-1){return true;}
                }
            }
        }
        return p;
    }
}
