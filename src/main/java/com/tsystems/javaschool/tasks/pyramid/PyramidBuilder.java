package com.tsystems.javaschool.tasks.pyramid;


import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    static boolean isPyramidExist(int len) {
        boolean ipe = false;
        if (len == 0) return false;
        else if (len == 1) return true;
        else {
            for (int i = 2; i < len; ) {
                int s = ((1 + i) * i) / 2;
                if (s < len) i++;
                else if (s == len) {
                    ipe = true;
                    break;
                } else if (s > len) {
                    ipe = false;
                    break;
                }
            }
            return ipe;
        }
    }


    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        // TODO : Implement your solution here
        int [][] pyramid;
        if(isPyramidExist(inputNumbers.size()) && !inputNumbers.contains(null)) {
            //sort the inputNumbers
            Collections.sort(inputNumbers);
            //i!=1 not a array , i != 2 - skip the value isPiramidExist
            int i = 2;
            int column, row = 0;
            //find count of rows
            while (i < inputNumbers.size()) {
                row++;
                i = ((row + 1) * row) / 2;
            }
            //find count of column
            column = 2 * row - 1;
            pyramid = new int[row][column];

            //fill the matrix
            int step = 0;
            for (int j = 0; j < row; j++) {
                for (int k = 0; k <= j; k++) {
                    int c;
                    c = column / 2 - j + 2 * k;
                    pyramid[j][c] = inputNumbers.get(step);
                    step++;
                }
            }
        }
        else
            throw new CannotBuildPyramidException();

        return pyramid;
    }


}
